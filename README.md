# Launching an ec2 instance that installs k3s on Alpine Linux using user-data 

The purpose of this documentation is to explain creating an aws image that can launch aws ec2 instances with k3s using user-data on Alpine Linux. Alpine linux has no configuration to use the user-data of an aws instance. You have to install some packages and make suitable set-up.


# dependencies in alpine linux:
python3 (for ansible)
py3-pip (for ansible)
boto3 (for aws)
curl (general - for k3s installation)
bash (for shh connection with openssh )
sqlite (?)
cloud-init (to use aws user-data)
openssh-server-pam (to connect ssh with PAM )

* Not to install dependencies in every launch we will create an image (aws ami) with those packages.

# Creating The Image

- First launch an ec2 instance:

> Region                     : us-east1 (N.Virginia)
> Amazon Machine Image (AMI) : alpine-ami-3.13.4-x86_64-r0 ami-01bced83ff9507b45
> Instance Type              : t2.micro (optional)
> Skip Configure Instance Details (use default)
> Add Storage                : 8 gb (by default alpine linux ami has 1 gb root volume)
> Configure Security Group   : choose/create a security group with shh 
> Launch instance

- Install the packages to newly created instance:
> connect to your instance with ssh
> type: 
    - sudo apk update
    - sudo apk upgrade

    - sudo apk add --no-cache python3 py3-pip
    - sudo pip3 install boto3

    - sudo apk add curl

    - sudo apk add bash 
    - sudo apk add sqlite (optional)

    - sudo apk add cloud-init
   * We need cloud-init to use ec2 user-data. Normally ec2 user-data scripts are stored in user-data.txt with this path:
   /var/lib/cloud/instance/user-data.txt 
   Before installing cloud-init you can check it. Type:
   ls /var/lib/cloud (you will see there is nothing under cloud folder)

>After the cloud-init package is installed run that command to prepare the OS for cloud-init use.
    - sudo setup-cloud-init

> Alpine linux asks for password when connecting with ssh. OpenSSH allows us to configure PAM to handle authentication duties.
    - sudo apk add openssh-server-pam 

> We need to edit /etc/ssh/sshd_config to ensure alpine not to ask password to ssh connection:
    - sudo vi /etc/ssh/sshd_config

    > change the configurationas below :
    #PasswordAuthentication no
    #ChallengeResponseAuthentication no
    EnablePAM yes (or UsePAM yes)
    > save the changes than reboot the instance

    - sudo reboot

* after starting again you can not access to instance because The fingerprint has changed. So this time go to aws console, stop ec2 instance than start again. Than copy new ssh connection command with newly created ip. connect to your instance now.

# for k3s in alpine (see k3s documantation);
* In order to pre-setup Alpine Linux you have to go through the following steps (if we don't do this after k3s installation the worker node not connects to server node):
sudo su
echo "cgroup /sys/fs/cgroup cgroup defaults 0 0" >> /etc/fstab 

cat >> /etc/cgconfig.conf <<EOF
mount {
cpuacct = /cgroup/cpuacct;
memory = /cgroup/memory;
devices = /cgroup/devices;
freezer = /cgroup/freezer;
net_cls = /cgroup/net_cls;
blkio = /cgroup/blkio;
cpuset = /cgroup/cpuset;
cpu = /cgroup/cpu;
}
EOF

default_kernel_opts="...  cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory"

update-extlinux

sudo reboot


# - Now we can create the image of the instance.
> go to aws console
> chhose instance you configurated
> Click "actions" menu  on top right 
> Click "image and templates" > "create image"
> On the  create image page fill the necassary places than click to "create image" button.

# Launching ec2 instance using userdata script

> Launch an ec2 instance with the "ami" you created.
> In Configure Instance Details, add the script below to userdata section:

    #!/bin/bash
    sudo curl -sfL https://get.k3s.io | sh -

* Userdata scripts are only used in first boot when launching the ec2 instance. Sometimes when using created ami's userdata scripts may not work because it the ami was created from a booted instance. In this case use this script:

#cloud-boothook
#!/bin/bash
sudo curl -sfL https://get.k3s.io | sh -



* If you can not connect via ssh because of sshd stopped, you can use userdata script to restart sshd:
> Go to aws console and stop ec2 instance
> Click actions>>instance settings>>edit user data
> Write below script to userdata
#cloud-boothook
#!/bin/bash
$ sudo iptables -F
$ sudo service sshd restart

> Start instance again

